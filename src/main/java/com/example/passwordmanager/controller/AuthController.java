package com.example.passwordmanager.controller;


import com.example.passwordmanager.cryptography.HashMethods;
import com.example.passwordmanager.model.User;
import com.example.passwordmanager.payload.ApiResponse;
import com.example.passwordmanager.payload.LoginRequest;
import com.example.passwordmanager.payload.LoginResponse;
import com.example.passwordmanager.payload.RegisterRequest;
import com.example.passwordmanager.repository.UserRepository;
import com.example.passwordmanager.utils.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AuthController {


    UserRepository userRepository;

    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/login")
    public LoginResponse loginUser(@RequestBody LoginRequest loginRequest) {
        String hashPassword;
        User userFromDb = userRepository.getUserByLogin(loginRequest.getLogin());
        if (userFromDb != null) {
            if (userFromDb.isPasswordKeptAsHash()) {
                hashPassword = HashMethods.calculateHMAC(loginRequest.getPassword() + userFromDb.getSalt() + Constants.PEPPER, Constants.HMAC_KEY);
            } else {
                hashPassword = HashMethods.calculateSHA512(loginRequest.getPassword() + userFromDb.getSalt() + Constants.PEPPER);
            }

            if (hashPassword.equals(userFromDb.getPasswordHash())) {
                return new
                        LoginResponse(userFromDb.getId(),
                                      userFromDb.getLogin(),
                                      userFromDb.getPasswordHash(),
                                      userFromDb.getSalt(),
                                      userFromDb.isPasswordKeptAsHash(),
                                      true);
            } else {
                return new LoginResponse(false);
            }
        } else {
            return new LoginResponse(false);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegisterRequest registerRequest) throws NoSuchAlgorithmException, NoSuchProviderException {
        String hashPassword;
        String salt = HashMethods.getSalt();
        if (registerRequest.isPasswordKeptAsHash()) {
            hashPassword = HashMethods.calculateHMAC(registerRequest.getPassword() + salt + Constants.PEPPER, Constants.HMAC_KEY);
        } else {
            hashPassword = HashMethods.calculateSHA512(registerRequest.getPassword() + salt + Constants.PEPPER);
        }
        User newUser = new User(registerRequest.getLogin(), hashPassword, salt, registerRequest.isPasswordKeptAsHash());
        userRepository.save(newUser);
        return ResponseEntity.ok(new ApiResponse(true, "Zarejestrowano"));
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<?> editUser(@PathVariable(name = "id") Long id, @RequestBody LoginRequest loginRequest) throws NoSuchAlgorithmException, NoSuchProviderException {


        String hashPassword;
        String salt = HashMethods.getSalt();
        if (userRepository.getUserById(id).isPasswordKeptAsHash()) {
            hashPassword = HashMethods.calculateHMAC(loginRequest.getPassword() + salt + Constants.PEPPER, Constants.HMAC_KEY);
        } else {
            hashPassword = HashMethods.calculateSHA512(loginRequest.getPassword() + salt + Constants.PEPPER);
        }

        userRepository.findById(id).map(newUser -> {
            newUser.setLogin(loginRequest.getLogin());
            newUser.setPasswordHash(hashPassword);
            newUser.setSalt(salt);
            return userRepository.save(newUser);
        });

        return ResponseEntity.ok(new ApiResponse(true, "Pomyślnie zmieniono"));
    }

    @GetMapping("/get/{id}")
    public User getUser(@PathVariable(name = "id") Long id) {
        return userRepository.getUserById(id);
    }

}
