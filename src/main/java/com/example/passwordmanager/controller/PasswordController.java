package com.example.passwordmanager.controller;


import com.example.passwordmanager.cryptography.HashMethods;
import com.example.passwordmanager.model.Password;
import com.example.passwordmanager.model.User;
import com.example.passwordmanager.payload.AddPasswordRequest;
import com.example.passwordmanager.payload.ApiResponse;
import com.example.passwordmanager.payload.PasswordSummary;
import com.example.passwordmanager.repository.PasswordRepository;
import com.example.passwordmanager.repository.UserRepository;
import com.example.passwordmanager.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/password")
public class PasswordController {

    UserRepository userRepository;

    PasswordRepository passwordRepository;

    public PasswordController(UserRepository userRepository, PasswordRepository passwordRepository) {
        this.userRepository = userRepository;
        this.passwordRepository = passwordRepository;
    }

    @GetMapping("/all/{id}")
    public List<PasswordSummary> getAllPasswords(@PathVariable(name = "id") Long id) {
        List<Password> passwords = passwordRepository.findAll();
        List<PasswordSummary> passwordSummaries = new ArrayList<>();

        passwords.forEach(password -> {
            passwordSummaries.add(new PasswordSummary(password.getId(),
                    password.getWebAddress(),
                    password.getDescription(),password.getLogin(), password.getUser().getId()));
        });
        Predicate<PasswordSummary> byUserId = password ->  password.getUserId() == id;
        return passwordSummaries.stream().filter(byUserId).collect(Collectors.toList());
    }


    @PostMapping("/add")
    public ResponseEntity<?> addPassword(@RequestBody AddPasswordRequest addPasswordRequest) {
        Key key;
        User currentUser = userRepository.getUserById(addPasswordRequest.getUserId());
        try {
          key = HashMethods.generateKey(currentUser.getPasswordHash());
          String encryptedPassword = HashMethods.encrypt(addPasswordRequest.getPassword(), key);
          Password newPassword = new Password(encryptedPassword, addPasswordRequest.getWebAddress(), addPasswordRequest.getDescription(), addPasswordRequest.getLogin());
          newPassword.setUser(currentUser);
          passwordRepository.save(newPassword);
          return ResponseEntity.ok(new ApiResponse(true, "Dodano hasło"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deletePassword(@PathVariable(name = "id") Long id) {
        passwordRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse(true, "Pomyślnie usunięto"));
    }


    @GetMapping("/decrypt/{id}")
    public ResponseEntity<?> decryptPassword(@PathVariable(name = "id") Long id) {
        Password password = passwordRepository.getPasswordById(id);
        User user = userRepository.getUserById(password.getUser().getId());
        String decryptedPassword;
        try {
            Key key = HashMethods.generateKey(user.getPasswordHash());
            decryptedPassword = HashMethods.decrypt(password.getPassword(), key);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok(new ApiResponse(true, decryptedPassword));
    }
}
