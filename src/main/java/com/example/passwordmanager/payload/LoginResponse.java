package com.example.passwordmanager.payload;

public class LoginResponse {
    private long id;

    private String login;

    private String passwordHash;

    private String salt;

    private boolean isPasswordKeptAsHash;

    private boolean success;

    public LoginResponse(boolean success) {
        this.success = success;
    }

    public LoginResponse(long id, String login, String passwordHash, String salt, boolean isPasswordKeptAsHash, boolean success) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.salt = salt;
        this.isPasswordKeptAsHash = isPasswordKeptAsHash;
        this.success = success;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isPasswordKeptAsHash() {
        return isPasswordKeptAsHash;
    }

    public void setPasswordKeptAsHash(boolean passwordKeptAsHash) {
        isPasswordKeptAsHash = passwordKeptAsHash;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
