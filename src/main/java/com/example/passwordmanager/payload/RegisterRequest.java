package com.example.passwordmanager.payload;

public class RegisterRequest {

    private String login;

    private String password;

    private boolean isPasswordKeptAsHash;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isPasswordKeptAsHash() {
        return isPasswordKeptAsHash;
    }

    public void setPasswordKeptAsHash(boolean passwordKeptAsHash) {
        isPasswordKeptAsHash = passwordKeptAsHash;
    }
}
