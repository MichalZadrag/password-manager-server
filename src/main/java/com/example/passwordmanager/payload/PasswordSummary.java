package com.example.passwordmanager.payload;

public class PasswordSummary {

    private long id;

    private String webAddress;

    private String description;

    private String login;

    private long userId;


    public PasswordSummary(long id, String webAddress, String description, String login, long userId) {
        this.id = id;
        this.webAddress = webAddress;
        this.description = description;
        this.login = login;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
